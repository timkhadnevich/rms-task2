package com.cipher;


import com.cipher.batch.EncodingBatchServiceImpl;
import com.cipher.model.BatchParametersInputObject;
import com.cipher.validation.ParametersValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController implements ErrorController {

    @Autowired
    EncodingBatchServiceImpl batchService;

    @Autowired
    ParametersValidationService parametersValidationService;


    private static final String PATH = "/error";

    @Value("${errormessage.allfields}")
    private String errorMessageAllFields;


    @GetMapping("/")
    public String root(Model model) {
        model.addAttribute("batchParametersInputObject", new BatchParametersInputObject());
        return "index";
    }

    @PostMapping("/encodeFile")
    public String encodeFile(@ModelAttribute BatchParametersInputObject batchParametersInputObject,
                             Model model) {
        String errorMessage = parametersValidationService.validateParameters(batchParametersInputObject);
        if(errorMessage != null) {
            model.addAttribute("errorMessage", errorMessage);
        } else {
            Long completionTime = batchService.createAndRunJob(batchParametersInputObject);
            model.addAttribute("successMessage", "Success. Completed in " + completionTime + " ms");
        }
        return "index";
    }

    @RequestMapping("/error")
    public String error(Model model) {
        model.addAttribute("errorMessage", errorMessageAllFields);
        return "error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}
