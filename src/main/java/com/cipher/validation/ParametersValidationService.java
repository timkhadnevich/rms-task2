package com.cipher.validation;

import com.cipher.model.BatchParametersInputObject;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ParametersValidationService {

    public String validateParameters(BatchParametersInputObject parametersInputObject) {
        File f = new File(parametersInputObject.getInputPath());
        if(!f.exists() || f.isDirectory()) {
            return "File doesn't exist or its directory.";
        }

        if( parametersInputObject.getNumberOfThreads() == null || parametersInputObject.getNumberOfThreads() == 0) {
            return "Provide valid number of threads.";
        }

        if(parametersInputObject.getNumberOfThreads() == null || parametersInputObject.getShift() == 0) {
            return "Provide valid left shift.";
        }
        return null;
    }

}
