package com.cipher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RMSTestTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RMSTestTwoApplication.class, args);
	}
}
