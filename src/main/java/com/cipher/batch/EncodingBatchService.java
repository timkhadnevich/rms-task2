package com.cipher.batch;

import com.cipher.model.BatchParametersInputObject;

/**
 * Service to run batch job to encode file in caesar cipher
 */
public interface EncodingBatchService {

    /**
     * Encodes the file by creating a new job
     * @param batchParametersInputObject
     * @return time took in ms to encode the file
     */
    Long createAndRunJob(BatchParametersInputObject batchParametersInputObject);
}
