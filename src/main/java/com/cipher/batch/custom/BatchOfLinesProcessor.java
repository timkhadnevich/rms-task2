package com.cipher.batch.custom;

import org.springframework.batch.item.ItemProcessor;

import java.util.ArrayList;
import java.util.List;

public class BatchOfLinesProcessor implements ItemProcessor<List<String>, List<String>> {

    final static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    int shift;

    public BatchOfLinesProcessor(int shift) {
        this.shift = shift;
    }


    @Override
    public List<String> process(List<String> lines) {

        ArrayList processedLines = new ArrayList<String>();
        lines.forEach(item -> {
            processedLines.add(processOneLine(item));
        });

        return processedLines;
    }


    private String processOneLine(String line) {
        StringBuilder resultLine = new StringBuilder();
        for (char c : line.toCharArray()) {
            if(Character.isAlphabetic(c)) {
                boolean isUpperCase = Character.isUpperCase(c);
                char lowerCase = Character.toLowerCase(c);
                int newPosition = BatchOfLinesProcessor.alphabet.indexOf(lowerCase) - shift;
                if(newPosition < 0) {
                    newPosition = BatchOfLinesProcessor.alphabet.length() + newPosition;
                }

                char targetChar = BatchOfLinesProcessor.alphabet.charAt(newPosition);

                if(isUpperCase) {
                    targetChar = Character.toUpperCase(targetChar);
                }
                resultLine.append(targetChar);
            } else {
                resultLine.append(c);
            }
        }

        return resultLine.toString();
    }
}
