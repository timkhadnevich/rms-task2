package com.cipher.batch.custom;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CustomFilePartitioner implements Partitioner {

    public static final String START_LINE = "startLine";
    public static final String END_LINE = "endLine";
    public static String PARTITION_KEY = "SINGLE_PARTITION_";



    private String inputResourceFile;

    public CustomFilePartitioner(String inputResourceFile) {
        this.inputResourceFile = inputResourceFile;
    }

    @Override
    public Map<String, ExecutionContext> partition(int partitionSize) {
        Map<String, ExecutionContext> map = new HashMap<>(partitionSize);

        try {
            Stream<String> lines = Files.lines(Paths.get(inputResourceFile), Charset.forName("UTF-8"));
            Long numberOflines = lines.count();
            Float numberOfPartitions = numberOflines.floatValue()/partitionSize;
            Double numberOfFullPartitions = Math.ceil(numberOfPartitions);
            for(int i=0; i < numberOfFullPartitions; i++) {
                ExecutionContext context = new ExecutionContext();
                context.put(START_LINE, i * partitionSize);

                if(i == (numberOfFullPartitions-1)) {
                    context.put(END_LINE, numberOflines);
                } else {
                    context.put(END_LINE, (i+1)*(partitionSize));
                }
                context.put("inputFilePath", inputResourceFile);
                String key = PARTITION_KEY + i;
                context.put("partitionKey", key);
                map.put(key, context);
            }

            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("Partiioner run");
        return map;
    }
}
