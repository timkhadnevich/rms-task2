package com.cipher.batch.custom;

import org.springframework.batch.item.ItemReader;
import org.springframework.lang.Nullable;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CustomFileItemReader implements ItemReader<List<String>> {

    private Integer startLine;
    private Integer endLine;
    private String fileName;
    boolean isProcessed = false;

    public CustomFileItemReader(Integer startLine,Integer endLine, String fileName) {
        this.startLine = startLine;
        this.endLine = endLine;
        this.fileName = fileName;
    }

    @Nullable
    @Override
    public List<String> read() throws Exception {

        if(isProcessed) {
            return null;
        }

        Stream<String> lines = Files.lines(Paths.get(fileName), Charset.forName("UTF-8"));
        List<String> linesToProcess = new ArrayList<>();
        lines.skip(startLine).limit(endLine - startLine).forEach(line -> linesToProcess.add(line));

        lines.close();
        isProcessed = true;
        return linesToProcess;
    }
}
