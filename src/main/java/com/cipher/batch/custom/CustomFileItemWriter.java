package com.cipher.batch.custom;


import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class CustomFileItemWriter implements ItemWriter<List<String>> {

    private String partitionKey;


    public CustomFileItemWriter(String partitionKey) {
        this.partitionKey = partitionKey;
    }


    @Override
    public void write(List<? extends List<String>> list) throws Exception {
        writeToTempFile(list);
    }

    private void writeToTempFile(List<? extends List<String>> list) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(
                partitionKey));
        for(String line: list.get(0)) {
            writer.write(line);
            writer.newLine();
        }

        writer.close();

        }

}
