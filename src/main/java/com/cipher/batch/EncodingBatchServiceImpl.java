package com.cipher.batch;

import com.cipher.batch.custom.CustomFileItemReader;
import com.cipher.batch.custom.CustomFileItemWriter;
import com.cipher.batch.custom.CustomFilePartitioner;
import com.cipher.batch.custom.BatchOfLinesProcessor;
import com.cipher.model.BatchParametersInputObject;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Service
@EnableBatchProcessing
@Configuration
public class EncodingBatchServiceImpl implements EncodingBatchService {


    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;


    @Override
    public Long createAndRunJob(BatchParametersInputObject batchParametersInputObject) {

        Job newJob = null;
        String inputFilePath = batchParametersInputObject.getInputPath();
        String outputFilePath = batchParametersInputObject.getOutputPath();
        Integer numberOfThreads =  batchParametersInputObject.getNumberOfThreads();
        Integer shift =  batchParametersInputObject.getShift();

        try {
            //Create job
            newJob = partitioningJob(numberOfThreads, inputFilePath, shift);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            long startTime = System.currentTimeMillis();

            //Run job
            JobExecution exec = jobLauncher.run(newJob, new JobParameters());
            while(!BatchStatus.COMPLETED.equals(exec.getStatus()));

            //Merge files when job is completed
            mergeFiles(outputFilePath);

            long estimatedTime = System.currentTimeMillis() - startTime;
            return estimatedTime;

        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }



    public Job partitioningJob(Integer numberOfThreads, String inputFilePath, Integer shift) throws Exception {
        return jobBuilderFactory.get("partitionerJob" + System.currentTimeMillis())
                .start(partitionStep(inputFilePath, numberOfThreads, shift))
                .build();
    }


    public Step partitionStep(String inputFilePath, Integer numberOfThreads, Integer shift) throws Exception {
        return stepBuilderFactory.get("partitionStep")
                .partitioner("slaveStep", new CustomFilePartitioner(inputFilePath))
                .step(slaveStep(shift))
                .taskExecutor(taskExecutor(numberOfThreads))
                .build();
    }


    @StepScope
    public Step slaveStep(Integer shift) throws Exception {
        return stepBuilderFactory.get("slaveStep")
                .<List<String>, List<String>>chunk(1)
                .reader(reader(null, null, null))
                .processor(processor(shift))
                .writer(writer(null))
                .build();
    }


    @StepScope
    @Bean
    public ItemReader<? extends List<String>> reader(@Value("#{stepExecutionContext[startLine]}") Integer startLine,
                                                     @Value("#{stepExecutionContext[endLine]}")  Integer endLine,
                                                     @Value("#{stepExecutionContext[inputFilePath]}")  String inputFilePath) throws IOException {
        return new CustomFileItemReader(startLine, endLine, inputFilePath);
    }

    @StepScope
    @Bean
    public ItemWriter<List<String>> writer(@Value("#{stepExecutionContext[partitionKey]}")  String partitionKey) {
        return new CustomFileItemWriter(partitionKey);
    }



    private BatchOfLinesProcessor processor(Integer shift) {
        return new BatchOfLinesProcessor(shift);
    }


    private TaskExecutor taskExecutor(int numberOfThreads) {
        SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
        taskExecutor.setConcurrencyLimit(numberOfThreads);
        return taskExecutor;
    }

    private void mergeFiles(String outputFile) throws IOException {
        File file = new File(outputFile);
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        Integer fileNumber = 0;
        String readFilePath = CustomFilePartitioner.PARTITION_KEY + fileNumber;
        while(Files.exists(Paths.get(readFilePath))) {
            try (Stream<String> stream = Files.lines(Paths.get(readFilePath))) {
                stream.forEach(line -> {
                    try {
                        writer.write(line);
                        writer.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }

            File fileToDelete = new File(CustomFilePartitioner.PARTITION_KEY + fileNumber);
            fileToDelete.delete();
            fileNumber++;
            readFilePath = CustomFilePartitioner.PARTITION_KEY + fileNumber;
        }
        writer.close();
    }


}
